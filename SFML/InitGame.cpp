#include "InitGame.hpp"

namespace Init{
    GameCore::GameCore(std::string name){
        this -> window = new sf::RenderWindow(sf::VideoMode(640,480),name);
    }
    void GameCore::Play(){
        this -> GameLoop();
    }
    
    void GameCore::GameLoop(){
        while(this -> window -> isOpen()){
            sf::Event event;
            this ->EventRunner(event);
            
        }
    }
    
    void GameCore::EventRunner(sf::Event event){
        while(this -> window -> pollEvent(event)){
            if(event.type == sf::Event::Closed){
                window -> close();
                
            }
        }
    }
    GameCore::~GameCore(){
        delete this -> window;
    }
}
