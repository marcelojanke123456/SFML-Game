#ifndef InitGame_hpp
#define InitGame_hpp

#include <iostream>
#include<SFML/Graphics.hpp>

namespace Init {
    
    class GameCore{
    public:
        GameCore(std::string name);
        void Play();
        void GameLoop();
        void EventRunner(sf::Event event);
        ~GameCore();
    private:
        sf::RenderWindow *window;
    };
    

    
}

#endif /* InitGame_hpp */
